//
//  ViewController.swift
//  Radar
//
//  Created by LBStek Inc. on 1/19/16.
//  Copyright © 2016 LBSTek Inc. All rights reserved.
//

import UIKit
import Just
import Haneke
import CoreLocation
import EasyAnimation

class ViewController: UIViewController, CLLocationManagerDelegate {
    var myView: UIView! = nil
    var backImageView: UIImageView! = nil
    var mugshotView: UIImageView! = nil
    var backgroundView: UIImageView! = nil
    var radarLight: UIImageView! = nil
    
    var locationManager: CLLocationManager! = nil
    var myLocation: CLLocation! = nil
    var markerViews: [UIImageView] = []
    var markerGroups: [[String : AnyObject]]! = nil
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingHeading()
        locationManager.startUpdatingLocation()
        
        let screenHeight = UIScreen.mainScreen().bounds.height
        let screenWidth = UIScreen.mainScreen().bounds.width
        
        let myViewWidth = screenHeight * 0.66 * 2
        let myViewHeight = screenHeight * 0.66 * 2
        let myViewX = -((screenHeight * 0.66) - (screenWidth * 0.5))
        let myViewY = (screenHeight * 0.33) - 30
        
        myView = UIView(frame: CGRect(x: myViewX, y: myViewY, width: myViewWidth, height: myViewHeight))
        //myView.layer.masksToBounds = true
        //myView.backgroundColor = UIColor.blueColor()
        // myView.layer.cornerRadius = myViewWidth * 0.5
        
        backImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: myViewWidth, height: myViewHeight))
        backImageView.image = UIImage(named: "radar")
        
        backgroundView = UIImageView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        backgroundView.image = UIImage(named: "background")
        
        radarLight = UIImageView(frame: CGRect(x: 0, y: 0, width: myViewWidth, height: myViewHeight))
        radarLight.image = UIImage(named: "radar_light")
        radarLight.transform = CGAffineTransformMakeScale(0.05, 0.05)
        
        UIView.animateWithDuration(4, delay: 1,
            options: [.Repeat, .AllowUserInteraction],
            animations: {
                self.radarLight.transform = CGAffineTransformMakeScale(1, 1)
            }, completion: nil)
        
        self.view.addSubview(backgroundView)
        
        myView.addSubview(backImageView)
        myView.addSubview(radarLight)
        self.view.addSubview(myView)
        
        print(self.view.subviews)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func handleTapRecognizer(recognizer: UITapGestureRecognizer) {
        // let tappedView = recognizer.view!
    }
    
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        let direction = newHeading.magneticHeading
        let radians = -direction / 180.0 * M_PI
        
        myView.transform = CGAffineTransformMakeRotation(CGFloat(radians))
        
        if markerViews.count > 0 {
            for marker in markerViews {
                marker.transform = CGAffineTransformMakeRotation(CGFloat(-radians))
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if myLocation == nil {
            myLocation = locations.first
            let myLat = myLocation.coordinate.latitude
            let myLng = myLocation.coordinate.longitude
            
            let params = [
                "longitude": myLng,
                "latitude": myLat
            ]
            
            let r = Just.post(
                "http://beta.l-somewhere.com:8000/mobile_api/account/radar/",
                data: params
            )
            
            if r.ok {
                print(r.json)
                let markers = r.json?.objectForKey("data") as! [[String : AnyObject]]
                
                var markerTag = 1
                
                for marker in markers {
                    let markerLat = marker["latitude"] as! Double
                    let markerLng = marker["longitude"] as! Double
                    let mugshot = marker["mugshot"] as! String
                    
                    let distance = CGFloat(myLocation.distanceFromLocation(CLLocation(latitude: markerLat, longitude: markerLng)))
                    print("distance: \(distance)")
                    
                    let xAxis = CGFloat(((markerLng - myLng) * 100000) / 9)
                    let yAxis = CGFloat(((markerLat - myLat) * 100000) / 9)
                    
                    print("xAxis: \(xAxis), yAxis: \(yAxis)")
                    
                    let markerX = (myView.frame.size.width * 0.5) + xAxis
                    let markerY = (myView.frame.size.height * 0.5) + yAxis
                    
                    print("markerX: \(markerX), markerY: \(markerY)")
                    
                    if markerX > 100 && markerY > 100 && markerX < myView.frame.size.width - 100 && markerY < myView.frame.size.height - 100  {
                        
                        let dotImage = UIImageView(frame: CGRect(x: myView.frame.size.width * 0.5, y: myView.frame.size.height * 0.5, width: 50, height: 50))
                        dotImage.hnk_setImageFromURL(NSURL(string: mugshot)!)
                        dotImage.layer.cornerRadius = 25
                        
                        UIView.animateWithDuration(2, delay: 2,
                            options: [.CurveEaseIn, .AllowUserInteraction],
                            animations: {
                            dotImage.frame.origin.x = markerX
                            dotImage.frame.origin.y = markerY
                        }, completion: nil)
                        
                        markerViews.append(dotImage)
                        myView.addSubview(dotImage)
                        
                        UIView.animateWithDuration(2, delay: 2,
                            options: [.Repeat, .Autoreverse, .CurveEaseIn, .AllowUserInteraction],
                            animations: {
                                dotImage.frame.size.height += 10
                                dotImage.frame.size.width += 10
                        }, completion: nil)
                        
                        dotImage.tag = markerTag
                        dotImage.userInteractionEnabled = true
                        
                        let tapRecong = UITapGestureRecognizer(target: self, action: Selector("handleTapRecognizer:"))
                        dotImage.addGestureRecognizer(tapRecong)
                        
                        markerTag += 1
                    }
                }
                
                var overlapCount = 0
                
                for markerView in markerViews {
                    for (var k=overlapCount+1; k<markerViews.count; k++) {
                        if CGRectIntersectsRect(markerView.frame, markerViews[k].frame) {
                            markerView.frame.origin.x = markerViews[k].frame.origin.x + 5
                            markerView.frame.origin.y = markerViews[k].frame.origin.y + 5
                        }
                    }
                    
                    overlapCount += 1
                }
                
            } else {
                print(r)
            }
        }

    }
}

